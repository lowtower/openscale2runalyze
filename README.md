# openScale2Runalyze

This simple python script reads out the weight data from an [openScale](https://github.com/oliexdev/openScale) database and loads it into a [Runalyze](https://runalyze.com) account.


- [openScale](https://github.com/oliexdev/openScale) is an open source weight and body metrics tracker, with support for bluetooth scales.
- [Runalyze](https://runalyze.com) is an online tool for training analysis.
