# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/de/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/lang/de/spec/v2.0.0.html).

## [0.4.1] - 2024-06-04

### Added

- enabled ruff for formatting and linting
- added python 3.12 to gitlab workflow

### Changed

- python 3.11 for testing, coverage and docs

### Removed

- removed python 3.8 from gitlab workflow

## [0.4.0] - 2022-11-10

### Added

- .editorconfig
- added python 3.10 testing in gitlab workflow
- setup.cfg
- automated documentation

### Changed

- reorganized project structure
  - moved python source to `src` directory 
  - formatting and linting
  - Makefile
  - moved config to setup.cfg

### Deprecated

### Removed

- removed python 3.7 testing in gitlab workflow

### Fixed

### Security

## [0.3.0] - 2021-09-10

### Added

- added multiple tests
- added "inspection" mode, which dumps the database contents 

### Changed

- some refactoring

### Deprecated

### Removed

### Fixed

### Security

## [0.2.1] - 2021-03-02

### Added

### Changed

- improved code quality (black, mypy, codespell)
- moved test to directory tests

### Deprecated

### Removed

### Fixed

### Security

## [0.2.0] - 2021-01-15

### Added

- simple user filter
- simple date filter
- simple debug mode

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.1.0] - 2021-01-13

### Added

- first version

### Changed

### Deprecated

### Removed

### Fixed

### Security
