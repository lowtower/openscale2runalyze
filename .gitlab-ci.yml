# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  SECURE_ANALYZERS_PREFIX: registry.gitlab.com/gitlab-org/security-products/analyzers
  DS_DEFAULT_ANALYZERS: bundler-audit, retire.js, gemnasium, gemnasium-maven, gemnasium-python
  DS_EXCLUDED_ANALYZERS: ''
  DS_EXCLUDED_PATHS: spec, test, tests, tmp
  DS_MAJOR_VERSION: 2
  LICENSE_MANAGEMENT_SETUP_CMD: ''
  LICENSE_MANAGEMENT_VERSION: 3
cache:
  paths:
  - ".cache/pip"
  - venv/
.pip_install_template: &pip_install_template
  - python -V
  - python -m pip install --upgrade pip
  - python -m pip install -r requirements.txt
  - python -m pip install -r requirements-dev.txt
.linting_template: &linting_template
  - black --line-length 120 --check --diff src tests
  - flake8 src tests
  - pylint src tests
  - mypy src tests
  - codespell README.md CHANGELOG.nd LICENSE src tests
  - ruff check src tests
stages:
- Static Analysis
- Test
- Documentation
- test
stan:pylint:39:
  image: python:3.9-alpine
  stage: Static Analysis
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - *linting_template
stan:pylint:310:
  image: python:3.10-alpine
  stage: Static Analysis
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - *linting_template
stan:pylint:311:
  image: python:3.11-alpine
  stage: Static Analysis
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - *linting_template
stan:pylint:312:
  image: python:3.12-alpine
  stage: Static Analysis
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - *linting_template
test:pytest:39:
  image: python:3.9-alpine
  stage: Test
  needs:
  - stan:pylint:39
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - python -m pytest --cov=src --cov-report=term tests
test:pytest:310:
  image: python:3.10-alpine
  stage: Test
  needs:
  - stan:pylint:310
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - python -m pytest --cov=src --cov-report=term tests
test:pytest:311:
  image: python:3.11-alpine
  stage: Test
  needs:
  - stan:pylint:311
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - python -m pytest  --junitxml=openscale2runalyze.xml --cov=src --cov-report=term tests
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    paths:
    - openscale2runalyze.xml
    reports:
      junit: openscale2runalyze.xml
      coverage_report:
        coverage_format: cobertura
        path: openscale2runalyze.xml
test:pytest:312:
  image: python:3.12-alpine
  stage: Test
  needs:
  - stan:pylint:312
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - python -m pytest --cov=src --cov-report=term tests

#stan:pylint:38:win_x86_64:
#  stage: Static Analysis
#  tags:
#     - shared-windows
#     - windows
#     - windows-1809
#  variables:
#    PYTHON_VERSION: "3.8"
#  before_script:
#    - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
#    - choco install python3 --version=$PYTHON_VERSION --yes --force --no-progress
#    - refreshenv
#    - *pip_install_template
#  script:
#  - *linting_template
#test:pytest:38:win_x86_64:
#  stage: Test
#  tags:
#    - shared-windows
#    - windows
#    - windows-1809
#  needs:
#    - stan:pylint:38:win_x86_64
#  variables:
#    PYTHON_VERSION: "3.8"
#  before_script:
#    - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
#    - choco install python3 --version=$PYTHON_VERSION --yes --force --no-progress
#    - refreshenv
#    - *pip_install_template
#  script:
#    - python -m pytest --cov=src --cov-report=term tests
docs:mkdocs:311:
  image: python:3.11-alpine
  stage: Documentation
  needs:
  - test:pytest:311
  before_script:
  - apk add --virtual linux-headers musl-dev gcc
  - *pip_install_template
  script:
  - python -m mkdocs build --clean --config-file ./mkdocs.yml
  artifacts:
    when: always
    paths:
      - site
sast:
  stage: test
  script:
  - echo "$CI_JOB_NAME is used for configuration only, and its script should not be
    executed"
  - exit 1
  artifacts:
    reports:
      sast: gl-sast-report.json
dependency_scanning_template:
  stage: test
  script:
  - echo "$CI_JOB_NAME is used for configuration only, and its script should not be
    executed"
  - exit 1
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  dependencies: []
  rules:
  - when: never
".ds-analyzer":
  extends: dependency_scanning_template
  allow_failure: true
  script:
  - "/analyzer run"
dependency_scanning:
  extends: ".ds-analyzer"
  image:
    name: "$DS_ANALYZER_IMAGE"
  variables:
    DS_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/gemnasium-python:$DS_MAJOR_VERSION"
  rules:
  - if: "$DEPENDENCY_SCANNING_DISABLED"
    when: never
  - if: "$DS_EXCLUDED_ANALYZERS =~ /gemnasium-python/"
    when: never
  - if: "$CI_COMMIT_BRANCH && $GITLAB_FEATURES =~ /\\bdependency_scanning\\b/ && $DS_DEFAULT_ANALYZERS
      =~ /gemnasium-python/"
    exists:
    - "{requirements.txt,*/requirements.txt,*/*/requirements.txt}"
    - "{requirements.pip,*/requirements.pip,*/*/requirements.pip}"
    - "{Pipfile,*/Pipfile,*/*/Pipfile}"
    - "{requires.txt,*/requires.txt,*/*/requires.txt}"
    - "{setup.py,*/setup.py,*/*/setup.py}"
  - if: "$CI_COMMIT_BRANCH && $GITLAB_FEATURES =~ /\\bdependency_scanning\\b/ && $DS_DEFAULT_ANALYZERS
      =~ /gemnasium-python/ && $PIP_REQUIREMENTS_FILE"
license_scanning:
  stage: test
  image:
    name: "$SECURE_ANALYZERS_PREFIX/license-finder:$LICENSE_MANAGEMENT_VERSION"
    entrypoint:
    - ''
  variables:
    LM_REPORT_VERSION: '2.1'
    SETUP_CMD: "$LICENSE_MANAGEMENT_SETUP_CMD"
  allow_failure: true
  script:
  - "/run.sh analyze ."
  artifacts:
    reports:
      license_scanning: gl-license-scanning-report.json
  dependencies: []
  rules:
  - if: "$LICENSE_MANAGEMENT_DISABLED"
    when: never
  - if: "$CI_COMMIT_BRANCH && $GITLAB_FEATURES =~ /\\blicense_scanning\\b/"
include:
- template: Security/SAST.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml
- template: Security/SAST-IaC.latest.gitlab-ci.yml
