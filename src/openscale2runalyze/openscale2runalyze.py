#!/usr/bin/env python
"""
Load weight data from an OpenScale sqlite database into Runalyze.com
"""
from __future__ import annotations

import argparse
import os
import re
import sqlite3
import sys
import time
from datetime import datetime

import pytz
import requests
from prettytable import from_db_cursor  # type: ignore

RUNALYZE_API_ENDPOINT = "https://runalyze.com/api/v1/"
__app_name__ = "openscale2runalyze"
__app_author__ = "lowtower"
__app_version__ = "0.4.0"

__sqlite_connection__ = None

REQUESTS_TIMEOUT = 600


class SystemException(Exception):
    """
    System exception instead of sys.exit()
    """


def main() -> None:
    """
    main function
    """
    debug = False

    args = parse_arguments(sys.argv[1:])
    headers = {"token": args.token}

    # activate debug mode
    if args.debug:
        debug = True

    # activate inspection mode
    if args.inspect_database:
        inspect_database()
    else:
        # check user id
        user_id = check_user_id(args.user_id)

        # read measurements from database and store in list of dictionaries
        measurements = read_sqlite_table_to_dict(user_id, args.newer_than)

        # loop through measurements, generate json content and load data to runalyze
        for measurement in measurements:
            json_content = {
                "date_time": measurement["date_time"],
                "weight": float(measurement["weight"]),
                "fat_percentage": float(measurement["fat_percentage"]),
                "water_percentage": float(measurement["water_percentage"]),
                "muscle_percentage": float(measurement["muscle_percentage"]),
                "bone_percentage": float(measurement["bone_percentage"]),
            }
            if debug:
                print(json_content)
            else:
                load_data_to_runalyze(json_content, headers)

    teardown_db()


def parse_arguments(args: list[str]) -> argparse.Namespace:
    """
    method to parse command line arguments

    Parameters:
        args: list of command line arguments
    Returns:
        argument parser namespace
    """
    parser = argparse.ArgumentParser(
        prog=__app_name__, description="Load weight data from an OpenScale sqlite database into Runalyze.com"
    )
    parser.add_argument(
        "-t",
        "--token",
        dest="token",
        metavar="TOKEN",
        type=check_runalyze_token,
        help="The personal API token for Runalyze.com (32 alphanumeric characters)",
    )
    parser.add_argument(
        "-d",
        "--database",
        dest="database",
        metavar="DATABASE",
        type=check_db,
        required=True,
        help="The sqlite database from an OpenScale export",
    )
    parser.add_argument(
        "-u",
        "--user-id",
        dest="user_id",
        metavar="ID",
        type=int,
        help="The openScale userId from which to load data to Runalyze",
    )
    parser.add_argument(
        "-n",
        "--newer-than",
        dest="newer_than",
        metavar="NEWER_THAN_DATE",
        type=check_openscale_date,
        help="Filter database to entries newer than date in format YYYYMMDD",
    )
    parser.add_argument(
        "-i",
        "--inspect-database",
        dest="inspect_database",
        action="store_true",
        help="Inspect the database.\nall other options are not evaluated!",
    )
    parser.add_argument("-dbg", "--debug", action="store_true", help="activate debug mode")
    parser.add_argument("-v", "--version", action="version", version=f"openscale2runalyze v{__app_version__}")
    return parser.parse_args(args)


def setup_db(path_to_db: str) -> bool:
    """
    Method for setting up the global database connection

    Parameters:
        path_to_db: path to openscale sqlite database
    Returns:
        True if the connection to the database has been established
    Raises:
        SystemException: sqlite connection fails
    """
    global __sqlite_connection__  # pylint: disable = global-statement
    try:
        __sqlite_connection__ = sqlite3.connect(path_to_db)
        print("\nThe SQLite connection has been established")
        print("-" * 40 + "\n")
        return True
    except sqlite3.Error as error:
        raise SystemException(f"The connection to the database could not be established!\n{error}") from error


def teardown_db() -> None:
    """
    Method for tearing down the global database connection

    Raises:
        AssertionError: sqlite connection fails
    """
    sqlite_connection = get_db()
    if not isinstance(sqlite_connection, sqlite3.Connection):
        raise AssertionError
    sqlite_connection.close()
    print("\n" + "-" * 40)
    print("The SQLite connection has been closed")


def get_db() -> sqlite3.Connection | None:
    """
    Method for getting the global database connection

    Returns:
        the sqlite connection handler if available
    """
    global __sqlite_connection__  # pylint: disable = global-statement, global-variable-not-assigned
    if isinstance(__sqlite_connection__, sqlite3.Connection):
        return __sqlite_connection__
    return None


def check_db(path_to_db: str) -> str:
    """
    Method for checking the given database path

    Parameters:
        path_to_db: path to the openscale sqlite database
    Returns:
        the path to the openscale sqlite database
    Raises:
        argparse.ArgumentTypeError: database doesn't exist or isn't readable
    """
    if os.path.isfile(path_to_db) and os.access(path_to_db, os.R_OK):
        setup_db(os.path.abspath(path_to_db))
    else:
        mes = f"The given database file < {path_to_db} > doesn't exist or isn't readable"
        raise argparse.ArgumentTypeError(mes)
    return path_to_db


def check_user_id(user_id: str) -> int:
    """
    Method for checking if the given user id exists in the database

    Parameters:
        user_id: user id
    Returns:
        user id as integer
    Raises:
        AssertionError: sqlite connection fails
        SystemException: the user id doesn't exist
    """
    sqlite_connection = get_db()
    if not isinstance(sqlite_connection, sqlite3.Connection):
        raise AssertionError
    try:
        cursor = sqlite_connection.cursor()
        cursor.execute("SELECT `id` from `scaleUsers` WHERE `id` == (?)", (user_id,))
        exists = cursor.fetchall()
        cursor.close()
        if not exists:
            raise SystemException(f"The given user id < {user_id} > doesn't exist in the database")
    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)

    return int(user_id)


def check_openscale_date(date: str, pat: re.Pattern = re.compile(r"^[0-9]{8}$")) -> int:
    """
    Method for verifying the date format

    Parameters:
        date: date
        pat: pattern to match token against
    Returns:
        the date as integer
    Raises:
        argparse.ArgumentTypeError: date is not valid
    """
    mes = f"The given date < {date} > is not valid - it should be eight integers (YYYYMMDD)"
    if not pat.match(date):
        raise argparse.ArgumentTypeError(mes)
    try:
        datetime(int(date[0:4]), int(date[4:6]), int(date[6:8]), tzinfo=pytz.UTC)
    except ValueError:
        raise argparse.ArgumentTypeError(mes) from ValueError
    return int(date)


def check_runalyze_token(token: str, pat: re.Pattern = re.compile(r"^[a-zA-Z0-9]{32}$")) -> str:
    """
    Method for verifying the runalyze api token with a regex

    Parameters:
        token: runalyze api token to check
        pat: pattern to match token against
    Returns:
        the token
    Raises:
        argparse.ArgumentTypeError: date is not valid
    """
    if not pat.match(token):
        mes = f"The given date < {token} > is not valid - it should be 32 alphanumeric characters"
        raise argparse.ArgumentTypeError(mes)
    return token


def load_data_to_runalyze(content: dict, headers: dict) -> None:
    """
    Load data to runalyze via api

    Parameters:
        content: json content for call to runalyze api
        headers: headers for call to runalyze api
    """
    request = requests.post(
        RUNALYZE_API_ENDPOINT + "metrics/bodyComposition", json=content, headers=headers, timeout=REQUESTS_TIMEOUT
    )
    print(request.content)


def inspect_database() -> None:
    """
    Inspect the database (print contents)

    Raises:
        AssertionError: sqlite connection fails
    """
    sqlite_connection = get_db()
    try:
        if not isinstance(sqlite_connection, sqlite3.Connection):
            raise AssertionError
        cursor = sqlite_connection.cursor()
        sql = (
            "SELECT `id`, `username`, strftime('%d.%m.%Y', (`birthday`/1000), 'unixepoch') as 'birthday', "
            "printf('%.2f', `bodyHeight`) as bodyHeight, `gender`, printf('%.2f', `initialWeight`) as initialWeight, "
            "printf('%.2f', `goalWeight`) as goalWeight, "
            "strftime('%d.%m.%Y', (`goalDate`/1000), 'unixepoch') as goalDate "
            "from scaleUsers"
        )
        cursor.execute(sql)
        print(from_db_cursor(cursor))
        sql = (
            "SELECT `id`, `userId`, `enabled`, strftime('%d.%m.%Y', (`datetime`/1000), 'unixepoch') as 'datetime', "
            "printf('%.2f', `weight`) as weight, printf('%.2f', `fat`) as fat, printf('%.2f', `water`) as water, "
            "printf('%.2f', `muscle`) as muscle, printf('%.2f', `bone`) as bone "
            "from scaleMeasurements"
        )
        cursor.execute(sql)
        print(from_db_cursor(cursor))
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)


def read_sqlite_table_to_dict(user_id: int | None = None, newer_than: int | None = None) -> list[dict[str, str]]:
    """
    Read sqlite database measurements into a dictionary and return it

    Parameters:
        user_id: filter database by user id
        newer_than: filter database by date
    Returns:
        a dict with filtered database entries
    Raises:
        AssertionError: sqlite connection fails
        SystemException: date is not valid
    """
    sqlite_connection = get_db()
    scale_measurements = []
    try:
        if not isinstance(sqlite_connection, sqlite3.Connection):
            raise AssertionError
        cursor = sqlite_connection.cursor()

        sqlite_select_query = "SELECT * from scaleMeasurements"
        where_queries = ["`enabled` = '1'"]
        if isinstance(user_id, int):
            where_queries.append(f"`userId` = '{str(user_id)}'")
        if isinstance(newer_than, int):
            try:
                date_timestamp = datetime.strptime(str(newer_than), "%Y%m%d")
            except ValueError as error:
                raise SystemException(f"The given date < {newer_than} > is no valid date!") from error
            timestamp = int(time.mktime(date_timestamp.timetuple())) * 1000
            where_queries.append(f"`datetime` > '{timestamp:d}'")
        if where_queries:
            sqlite_select_query += " WHERE " + " AND ".join(where_queries)
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        for row in records:
            readable_date = (
                datetime.fromtimestamp(int(row[3] / 1000), tz=pytz.UTC).isoformat(timespec="seconds")[0:19] + "Z"
            )
            scale_measurement = {
                "date_time": readable_date,
                "weight": f"{float(row[4]):0.2f}",
                "fat_percentage": f"{float(row[5]):0.2f}",
                "water_percentage": f"{float(row[6]):0.2f}",
                "muscle_percentage": f"{float(row[7]):0.2f}",
                "bone_percentage": f"{float(100 * row[12] / row[4]):0.2f}",
            }
            scale_measurements.append(scale_measurement)
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        num_entries = 0
        if scale_measurements:
            num_entries = len(scale_measurements)
        print(f"Number of database entries:\t{num_entries}\n")

    return scale_measurements


if __name__ == "__main__":
    try:
        main()
    except SystemException as sys_error:
        print(sys_error)
