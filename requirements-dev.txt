# Runtime requirements
--requirement requirements.txt

# Testing
coverage
pytest
pytest-cov
pytest-mock

# Linting/Tooling
autopep8
black
codespell
flake8
isort
mypy
pre-commit
pur
pycodestyle
pyflakes
pylint
python-dateutil
ruff

# Documentation
griffe
mkdocs
mkdocstrings
mkdocstrings-python
mkdocs-gen-files
mkdocs-literate-nav
mkdocs-material
mkdocs-section-index
Pygments
