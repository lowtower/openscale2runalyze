"""Generate the code reference pages and navigation."""
from __future__ import annotations

from pathlib import Path

import mkdocs_gen_files
from griffe.collections import ModulesCollection
from griffe.loader import GriffeLoader

TOP_LEVEL_NAME = "openscale2runalyze"
DIRECTORY = "reference"
SRC = "src"


def remove_top_level_domain(path: Path, remove_part: str = TOP_LEVEL_NAME) -> list:
    """
    Remove TOP_LEVEL_NAME from pathlib.Path

    Parameters:
        path: Path object to remove a part of
        remove_part: part to remove from Path object

    Returns:
        List of reduced parts of Path
    """
    return [z for z in list(path.parts) if z != remove_part]


def main() -> None:
    """
    main entry point
    """

    collection = ModulesCollection()
    loader = GriffeLoader(modules_collection=collection)
    loader.load_module(Path(SRC, TOP_LEVEL_NAME))

    nav = mkdocs_gen_files.Nav()

    for path in sorted(Path(SRC).rglob("*.py")):
        module_path = path.relative_to(SRC).with_suffix("")

        doc_path = path.relative_to(SRC).with_suffix(".md")
        full_doc_path = Path(DIRECTORY, doc_path)
        doc_path = Path(*remove_top_level_domain(doc_path))
        full_doc_path = Path(*remove_top_level_domain(full_doc_path))

        parts = list(module_path.parts)
        # omit __init__, __main__, cli.py
        if parts[-1] in ["__init__", "__main__", "cli"] or not collection[module_path.parts].has_docstrings:
            continue

        if not parts:
            continue

        for_nav = tuple(remove_top_level_domain(module_path))
        nav[for_nav] = doc_path.as_posix()

        with mkdocs_gen_files.open(full_doc_path, "w") as file_handle:
            ident = ".".join(parts)
            file_handle.write(f"::: {ident}")

        # mkdocs_gen_files.set_edit_path(full_doc_path, path)
        mkdocs_gen_files.set_edit_path(full_doc_path, Path("../") / path)

    with mkdocs_gen_files.open(f"{DIRECTORY}/SUMMARY.md", "w") as nav_file:
        nav_file.writelines(nav.build_literate_nav())


if __name__ == "__main__":
    main()
