PROJECT=openscale2runalyze
SRC_CORE=src
SRC_TEST=tests
SRC_COMPLETE=$(SRC_CORE) $(SRC_TEST)
PYTHON=python3

help: ## Print help for each target
	$(info Makefile low-level Python API.)
	$(info =============================)
	$(info )
	$(info Available commands:)
	$(info )
	@grep '^[[:alnum:]_-]*:.* ##' $(MAKEFILE_LIST) \
		| sort | awk 'BEGIN {FS=":.* ## "}; {printf "%-25s %s\n", $$1, $$2};'

clean: ## Cleanup
	@echo "cleanup" 
	@rm -f  ./*.pyc
	@rm -rf ./__pycache__
	@rm -f  $(SRC_CORE)/*.pyc
	@rm -rf $(SRC_CORE)/__pycache__
	@rm -f  $(SRC_TEST)/*.pyc
	@rm -rf $(SRC_TEST)/__pycache__
	@rm -rf ./.coverage
	@rm -rf ./coverage.xml
	@rm -rf ./.pytest_cache
	@rm -rf ./.mypy_cache
	@rm -rf ./reports
	@rm -rf ./site

setup: ## Setup virtual environment
	$(PYTHON) -m venv .env
	.env/bin/pip install --upgrade pip wheel
	.env/bin/pip install --upgrade -r requirements.txt
	.env/bin/pip install --upgrade -r requirements-dev.txt

auto-style: ## Style the code
	@if type isort >/dev/null 2>&1 ; then echo "isort" ; .env/bin/python -m isort $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install isort' first." >&2 ; fi
	@if type autopep8 >/dev/null 2>&1 ; then echo "autopep8" ; .env/bin/python -m autopep8 -i -r $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install autopep8' first." >&2 ; fi
	@if type black >/dev/null 2>&1 ; then echo "black" ; .env/bin/python -m black --line-length 120 $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install black' first." >&2 ; fi
	@if type ruff >/dev/null 2>&1 ; then echo "ruff" ; .env/bin/ruff check --fix $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install ruff' first." >&2 ; fi

code-style: ## Check the code style
	@if type pycodestyle >/dev/null 2>&1 ; then echo "pycodestyle" ;  .env/bin/python -m pycodestyle --max-line-length=120 $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install pycodestyle' first." >&2 ; fi
	@if type isort >/dev/null 2>&1 ; then echo "isort" ; .env/bin/python -m isort $(SRC_COMPLETE) --check-only ; \
	 else echo "SKIPPED. Run '.env/bin/pip install isort' first." >&2 ; fi
	@if type black >/dev/null 2>&1 ; then echo "black" ; .env/bin/python -m black --line-length 120 $(SRC_COMPLETE) --check ; \
	 else echo "SKIPPED. Run '.env/bin/pip install black' first." >&2 ; fi
	@if type ruff >/dev/null 2>&1 ; then echo "ruff" ; .env/bin/ruff check $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install ruff' first." >&2 ; fi

format: auto-style ## Check the code style

code-lint: ## Lint the code
	@if type pyflakes >/dev/null 2>&1 ; then echo "pyflakes"; .env/bin/python -m pyflakes $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install pyflakes' first." >&2 ; fi
	@if type pylint >/dev/null 2>&1 ; then echo "pylint"; .env/bin/python -m pylint $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install pylint' first." >&2 ; fi
	@if type flake8 >/dev/null 2>&1 ; then echo "flake8"; .env/bin/python -m flake8 --max-complexity 10 $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install flake8' first." >&2 ; fi
	@if type mypy >/dev/null 2>&1 ; then echo "mypy"; .env/bin/python -m mypy $(SRC_COMPLETE) ; \
	 else echo "SKIPPED. Run '.env/bin/pip install mypy' first." >&2 ; fi

code-spell: ## Spelling of the Code
	@if type codespell >/dev/null 2>&1 ; then echo "codespell" ;  codespell $(SRC_COMPLETE) README.md CHANGELOG.md ; \
	 else echo "SKIPPED. Run '.env/bin/pip install codespell' first." >&2 ; fi

test: ## Test the code
	@type pytest >/dev/null 2>&1 || (echo "Run '.env/bin/pip install pytest' first." >&2 ; exit 1)
	.env/bin/python -m pytest tests

test-debug: ## Test only debug tests
	@type pytest >/dev/null 2>&1 || (echo "Run '.env/bin/pip install pytest' first." >&2 ; exit 1)
	.env/bin/python -m pytest -vv -ra -s -m debug tests

coverage: ## Coverage for the code
	@type coverage >/dev/null 2>&1 || (echo "Run '.env/bin/pip install coverage' first." >&2 ; exit 1)
	.env/bin/python -m pytest --cov=./$(SRC_CORE) --cov-branch --cov-report=term --cov-report=html tests

docs: ## Generate documentation
	@if type mkdocs >/dev/null 2>&1 ; then .env/bin/python -m mkdocs build -v --clean --config-file ./mkdocs.yml ; \
	 else echo "SKIPPED. Run '.env/bin/pip install mkdocs' first." >&2 ; fi

lint: code-style code-lint code-spell ## Linting Workflow - Code style, Linting, Spelling

workflow: clean setup lint coverage docs ## Complete Workflow - Linting, testing, build documentation

deps-update: ## Update the dependencies
	@if type pur >/dev/null 2>&1 ; then .env/bin/python -m pur -r requirements-dev.txt ; \
	 else echo "SKIPPED. Run '.env/bin/pip install pur' first." >&2 ; fi

deps-install: ## Install the dependencies
	@type .env/bin/pip >/dev/null 2>&1 || (echo "Run 'curl https://bootstrap.pypa.io/get-pip.py|sudo python3' first." >&2 ; exit 1)
	@.env/bin/pip install -r requirements.txt

.PHONY: auto-style format clean code-lint lint code-spell code-style coverage deps-install deps-update docs help test setup
