#!/usr/bin/env python
"""
Tests for openscale2runalyze
"""
from __future__ import annotations

import re
import sqlite3
from argparse import ArgumentTypeError
from io import StringIO
from unittest.mock import MagicMock, patch

import pytest

from src.openscale2runalyze import openscale2runalyze


@patch("sys.stderr", new_callable=StringIO)
def test_parser_missing_database_raises_system_exit_exception(mock_stderr: MagicMock) -> None:
    """
    test missing database argument
    """
    args = ["--token", "0123456789a0123456789b0123456789"]
    with pytest.raises(SystemExit):
        openscale2runalyze.parse_arguments(args)
    assert re.search(
        r"openscale2runalyze: error: the following arguments are required: -d/--database", mock_stderr.getvalue()
    )


@pytest.mark.parametrize(
    "invalid_token",
    [
        "0123456789",
        "0123456789a0123456789b0123456789c0123456789",
        "^123456789a0123456789b012345678$",
    ],
)
@patch("sys.stderr", new_callable=StringIO)
def test_parser_invalid_token_raises_system_exit_exception(mock_stderr: MagicMock, invalid_token: str) -> None:
    """
    test various invalid tokens:
        - too short
        - too long
        - invalid characters
    """
    args = ["--token", invalid_token, "--database", "tests/assets/test.db"]
    with pytest.raises(SystemExit):
        openscale2runalyze.parse_arguments(args)
    assert re.search(r"openscale2runalyze: error: argument -t/--token", mock_stderr.getvalue())


@pytest.mark.parametrize(
    "invalid_date",
    [
        "202006",
        "2020062305",
        "^2020-06-23",
    ],
)
@patch("sys.stderr", new_callable=StringIO)
def test_parser_invalid_date_format_raises_system_exit_exception(mock_stderr: MagicMock, invalid_date: str) -> None:
    """
    test various invalid date formats:
        - too short
        - too long
        - invalid characters
    """
    args = [
        "--token",
        "0123456789a0123456789b0123456789",
        "--database",
        "tests/assets/test.db",
        "--newer-than",
        invalid_date,
    ]
    with pytest.raises(SystemExit):
        openscale2runalyze.parse_arguments(args)
    assert re.search("openscale2runalyze: error: argument -n/--newer-than", mock_stderr.getvalue())


@pytest.mark.parametrize(
    "invalid_date",
    [
        "20209901",
        "20200199",
        "20200231",
    ],
)
@patch("sys.stderr", new_callable=StringIO)
def test_parser_invalid_dates_raise_value_error(mock_stderr: MagicMock, invalid_date: str) -> None:
    """
    test various invalid dates:
        - wrong month
        - wrong day
        - wrong month day
    """
    args = [
        "--token",
        "0123456789a0123456789b0123456789",
        "--database",
        "tests/assets/test.db",
        "--newer-than",
        invalid_date,
    ]
    with pytest.raises(SystemExit):
        openscale2runalyze.parse_arguments(args)
    assert re.search("openscale2runalyze: error: argument -n/--newer-than", mock_stderr.getvalue())


@patch("sqlite3.connect")
def test_setup_db_returns_true(sqlite_connection: MagicMock, setup_db: sqlite3.Connection) -> None:
    """
    test for setting up the database
    """
    sqlite_connection.return_value = setup_db
    assert openscale2runalyze.setup_db("arbitrary_string") is True


def test_setup_db_with_wrong_path_raises_exception() -> None:
    """
    test for setting up the database with exception
    """
    invalid_path = "/this/path/does_not/exist.db"
    with pytest.raises(openscale2runalyze.SystemException):
        openscale2runalyze.setup_db(invalid_path)


def test_check_db_with_invalid_path_raises_exception() -> None:
    """
    test for invalid path for sqlite database
    """
    invalid_path = "/this/path/does_not/exist.db"
    with pytest.raises(ArgumentTypeError):
        openscale2runalyze.check_db(invalid_path)


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_check_user_id_does_exist(sqlite_connection: MagicMock, setup_db: sqlite3.Connection) -> None:
    """
    test for existing users in database
    """
    sqlite_connection.return_value = setup_db

    # user id exists
    user_id = openscale2runalyze.check_user_id("1")
    assert user_id == 1


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_check_user_id_does_not_exist_raises_exception(
    sqlite_connection: MagicMock, setup_db: sqlite3.Connection
) -> None:
    """
    test for not existing users in database
    """
    sqlite_connection.return_value = setup_db

    # user id exists NOT
    with pytest.raises(openscale2runalyze.SystemException):
        openscale2runalyze.check_user_id("99")


def test_check_valid_date_returns_integer_date() -> None:
    """
    test a valid date format to return an integer
    """
    valid_date = "20210909"
    assert isinstance(openscale2runalyze.check_openscale_date(valid_date), int)
    assert openscale2runalyze.check_openscale_date(valid_date) == 20210909


# @pytest.mark.debug
@patch("sys.stdout", new_callable=StringIO)
@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_inspect_db_returns_values(
    sqlite_connection: MagicMock, mock_stdout: MagicMock, setup_db: sqlite3.Connection
) -> None:
    """
    test for inspecting the database
    """
    sqlite_connection.return_value = setup_db

    expected_out = (
        "+----+----------+------------+------------+--------+---------------+------------+------------+\n"
        "| id | username |  birthday  | bodyHeight | gender | initialWeight | goalWeight |  goalDate  |\n"
        "+----+----------+------------+------------+--------+---------------+------------+------------+\n"
        "| 1  |  USER_1  | 22.06.1974 |   190.00   |   0    |     75.00     |   70.00    | 31.12.2021 |\n"
        "| 2  |  USER_2  | 28.07.1975 |   165.00   |   1    |     60.00     |   50.00    | 31.12.2021 |\n"
        "+----+----------+------------+------------+--------+---------------+------------+------------+\n"
        "+----+--------+---------+------------+--------+-------+-------+--------+-------+\n"
        "| id | userId | enabled |  datetime  | weight |  fat  | water | muscle |  bone |\n"
        "+----+--------+---------+------------+--------+-------+-------+--------+-------+\n"
        "| 1  |   1    |    1    | 10.07.2018 | 71.00  | 11.00 | 60.00 | 44.00  | 12.00 |\n"
        "| 2  |   1    |    1    | 27.09.2020 | 67.85  |  8.70 | 63.30 | 45.60  | 10.90 |\n"
        "| 3  |   2    |    1    | 09.09.2021 | 49.00  | 12.00 | 58.00 | 40.00  | 12.00 |\n"
        "+----+--------+---------+------------+--------+-------+-------+--------+-------+\n"
    )
    openscale2runalyze.inspect_database()
    assert mock_stdout.getvalue() == expected_out


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_read_sqlite_table_to_dict_with_invalid_date_raises_exception(
    sqlite_connection: MagicMock, setup_db: sqlite3.Connection
) -> None:
    """
    test for retrieving the full data from the database with invalid date
    """
    sqlite_connection.return_value = setup_db

    with pytest.raises(openscale2runalyze.SystemException):
        openscale2runalyze.read_sqlite_table_to_dict(newer_than=20211332)


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_read_sqlite_table_to_dict_returns_full_dict(
    sqlite_connection: MagicMock, setup_db: sqlite3.Connection, expected_output: list
) -> None:
    """
    test for retrieving the full data from the database
    """
    sqlite_connection.return_value = setup_db
    expected_out = expected_output
    output = openscale2runalyze.read_sqlite_table_to_dict()
    assert output == expected_out


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_read_sqlite_table_to_dict_with_newer_than_returns_filtered_dict(
    sqlite_connection: MagicMock, setup_db: sqlite3.Connection, expected_output: list
) -> None:
    """
    test for retrieving date filtered data from the database
    """
    sqlite_connection.return_value = setup_db
    expected_out = [expected_output[1], expected_output[2]]
    output = openscale2runalyze.read_sqlite_table_to_dict(newer_than=20200101)
    assert output == expected_out


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_read_sqlite_table_to_dict_with_user_id_returns_filtered_dict(
    sqlite_connection: MagicMock, setup_db: sqlite3.Connection, expected_output: list
) -> None:
    """
    test for retrieving user id filtered data from the database
    """
    sqlite_connection.return_value = setup_db
    expected_out = [expected_output[0], expected_output[1]]
    output = openscale2runalyze.read_sqlite_table_to_dict(1)
    assert output == expected_out


@patch("src.openscale2runalyze.openscale2runalyze.get_db")
def test_read_sqlite_table_to_dict_with_user_id_and_newer_than_returns_filtered_dict(
    sqlite_connection: MagicMock,
    setup_db: sqlite3.Connection,
    expected_output: list,
) -> None:
    """
    test for retrieving user id filtered data from the database
    """
    sqlite_connection.return_value = setup_db
    expected_out = [expected_output[1]]
    output = openscale2runalyze.read_sqlite_table_to_dict(1, 20200101)
    assert output == expected_out
