"""
Fixtures for openscale2runalyze
"""

from __future__ import annotations

import sqlite3
from collections.abc import Generator

import pytest


@pytest.fixture(name="connection")
def fixture_connection() -> Generator:
    """SQLite session"""
    connection = sqlite3.connect("file::memory:?cache=shared", uri=True)
    yield connection
    connection.close()


@pytest.fixture(name="setup_db")
def fixture_setup_db(connection: sqlite3.Connection) -> Generator:
    """
    Set up a temporary database

    Args:
        connection (sqlite3.Connection): an in-memory SQLite connection
    Returns:
        Generator: SQLite connection
    """
    session = connection.cursor()
    # self.maxDiff = 5000
    sql = "DROP TABLE IF EXISTS `scaleUsers`;"
    session.execute(sql)
    sql = (
        "CREATE TABLE `scaleUsers` "
        "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
        "`username` TEXT NOT NULL, "
        "`birthday` INTEGER NOT NULL, "
        "`bodyHeight` REAL NOT NULL, "
        "`gender` INTEGER NOT NULL, "
        "`initialWeight` REAL NOT NULL, "
        "`goalWeight` REAL NOT NULL, "
        "`goalDate` INTEGER);"
    )
    session.execute(sql)
    sql = (
        "INSERT INTO 'scaleUsers' "
        "    (`id`, `username`, `birthday`, `bodyHeight`, `gender`, `initialWeight`, `goalWeight`, `goalDate`) "
        "VALUES "
        "    (1, 'USER_1', 141174000000, 190.0, 0, 75.0, 70.0, 1640991599000);"
    )
    session.execute(sql)
    sql = (
        "INSERT INTO 'scaleUsers' "
        "    (`id`, `username`, `birthday`, `bodyHeight`, `gender`, `initialWeight`, `goalWeight`, `goalDate`) "
        "VALUES "
        "    (2, 'USER_2', 175820400000, 165.0, 1, 60.0, 50.0, 1640991599000);"
    )
    session.execute(sql)
    sql = "DROP TABLE IF EXISTS `scaleMeasurements`;"
    session.execute(sql)
    sql = (
        "CREATE TABLE `scaleMeasurements` ("
        "   `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "   `userId` INTEGER NOT NULL,"
        "   `enabled` INTEGER NOT NULL,"
        "   `datetime` INTEGER,"
        "   `weight` REAL NOT NULL,"
        "   `fat` REAL NOT NULL,"
        "   `water` REAL NOT NULL,"
        "   `muscle` REAL NOT NULL,"
        "   `visceralFat` REAL NOT NULL,"
        "   `lbm` REAL NOT NULL,"
        "   `waist` REAL NOT NULL,"
        "   `hip` REAL NOT NULL,"
        "   `bone` REAL NOT NULL,"
        "   `chest` REAL NOT NULL,"
        "   `thigh` REAL NOT NULL,"
        "   `biceps` REAL NOT NULL,"
        "   `neck` REAL NOT NULL,"
        "   `caliper1` REAL NOT NULL,"
        "   `caliper2` REAL NOT NULL,"
        "   `calories` REAL NOT NULL,"
        "   FOREIGN KEY(`userId`) REFERENCES `scaleUsers`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE"
        ");"
    )
    session.execute(sql)
    sql = (
        "CREATE UNIQUE INDEX"
        "   `index_scaleMeasurements_userId_datetime` ON `scaleMeasurements` (`userId`, `datetime`); "
    )
    session.execute(sql)
    sql = (
        "INSERT INTO 'scaleMeasurements' "
        "    ( `id`, `userId`, `enabled`, `datetime`, `weight`, `fat`, `water`, `muscle`, `visceralFat`, `lbm`, "
        "`waist`, `hip`, `bone`, `chest`, `thigh`, `biceps`, `neck`, `caliper1`, `caliper2`, `calories`) "
        "VALUES "
        "    (1, 1, 1, 1531186955000, 71.00, 11.00, 60.00, 44.00, 0, 0, 0, 0, 12.00, 0, 0, 0, 0, 0, 0, 0);"
    )
    session.execute(sql)
    sql = (
        "INSERT INTO 'scaleMeasurements' "
        "    ( `id`, `userId`, `enabled`, `datetime`, `weight`, `fat`, `water`, `muscle`, `visceralFat`, `lbm`, "
        "`waist`, `hip`, `bone`, `chest`, `thigh`, `biceps`, `neck`, `caliper1`, `caliper2`, `calories`) "
        "VALUES "
        "    (2, 1, 1, 1601186955000, 67.85, 8.70, 63.30, 45.60, 0, 0, 0, 0, 10.90, 0, 0, 0, 0, 0, 0, 0);"
    )
    session.execute(sql)
    sql = (
        "INSERT INTO 'scaleMeasurements' "
        "    ( `id`, `userId`, `enabled`, `datetime`, `weight`, `fat`, `water`, `muscle`, `visceralFat`, `lbm`, "
        "`waist`, `hip`, `bone`, `chest`, `thigh`, `biceps`, `neck`, `caliper1`, `caliper2`, `calories`) "
        "VALUES "
        "    (3, 2, 1, 1631186955000, 49.00, 12.00, 58.00, 40.00, 0, 0, 0, 0, 12.00, 0, 0, 0, 0, 0, 0, 0);"
    )
    session.execute(sql)

    session.connection.commit()

    yield connection
    connection.close()


@pytest.fixture(name="expected_output")
def fixture_expected_output() -> list:
    """Return the expected output

    Returns:
        List: List with expected output"""
    expected_output_dict = [
        {
            "date_time": "2018-07-10T01:42:35Z",
            "weight": "71.00",
            "fat_percentage": "11.00",
            "water_percentage": "60.00",
            "muscle_percentage": "44.00",
            "bone_percentage": "16.90",
        },
        {
            "date_time": "2020-09-27T06:09:15Z",
            "weight": "67.85",
            "fat_percentage": "8.70",
            "water_percentage": "63.30",
            "muscle_percentage": "45.60",
            "bone_percentage": "16.06",
        },
        {
            "date_time": "2021-09-09T11:29:15Z",
            "weight": "49.00",
            "fat_percentage": "12.00",
            "water_percentage": "58.00",
            "muscle_percentage": "40.00",
            "bone_percentage": "24.49",
        },
    ]
    return expected_output_dict
